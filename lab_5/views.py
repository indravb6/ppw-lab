from django.shortcuts import render, redirect, HttpResponse
from django.http import QueryDict, JsonResponse
from .models import Schedule
import json


def add(request):
    if request.method == "POST":
        data = dict(request.POST.items())
        del data['csrfmiddlewaretoken']
        schedule = Schedule(**data)
        try:
            schedule.full_clean()
            schedule.save()
        except Exception as e:
            e = {k: v[0] for k, v in e.message_dict.items()}
            return render(request, 'lab5_add.html', e)
    return render(request, 'lab5_add.html', {})


def index(request):
    return render(request, 'lab5_index.html', {"data": list(Schedule.objects.all())})


def delete(request):
    if request.method == "POST":
        Schedule.objects.all().delete()
    return index(request)
