from django.urls import re_path, path
from .views import add, index, delete
# url for app
urlpatterns = [
    re_path(r'^add$', add, name='add'),
    re_path(r'^delete$', delete, name='delete'),
    re_path(r'^$', index, name='index'),
]
