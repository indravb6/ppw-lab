from django.db import models

class Schedule(models.Model):
    DAY_NAMES = (
        ("Sunday", "Sunday"),
        ("Monday", "Monday"),
        ("Tuesday", "Tuesday"),
        ("Wednesday", "Wednesday"),
        ("Thursday", "Thursday"),
        ("Friday", "Friday"),
        ("Saturday", "Saturday"),
    )
    name = models.CharField(max_length=32)
    day = models.CharField(max_length=9, choices=DAY_NAMES)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=32)
    category = models.CharField(max_length=32)
