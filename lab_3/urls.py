from django.urls import re_path, path
from .views import index, is_email_available
# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
]
