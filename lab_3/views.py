from django.shortcuts import render
from django.http import JsonResponse
from .models import Guest


def index(request):
    return render(request, 'lab3_index.html', {})


def register_page(request):
    if request.method == "POST":
        data = dict(request.POST.items())
        del data['csrfmiddlewaretoken']
        guest = Guest(**data)
        try:
            guest.full_clean()
            guest.save()
        except Exception as e:
            e = [v[0] for k, v in e.message_dict.items()]
            return JsonResponse({"error": e}, status=400)
        return JsonResponse({"message": "Data berhasil disimpan"})
    else:
        return render(request, 'lab10_register.html', {})


def is_email_available(request):
    email = request.GET["email"]
    emails = Guest.objects.filter(email=email).all()
    return JsonResponse({"email": email, "available": not bool(emails)})


def guest_list(request):
    return render(request, "lab10_list.html", {})


def get_list(request):
    guests = Guest.objects.all()
    data = [{"id": o.id, "name": o.name, "email": o.email}
            for o in list(guests)]
    return JsonResponse({"data": data})


def delete_guest(request):
    gid = request.POST['id']
    password = request.POST['password']
    guests = Guest.objects.filter(id=gid, password=password).all()
    if guests:
        guests[0].delete()
        return JsonResponse({"message": "Data berhasil dihapus"})
    return JsonResponse({"error": "Data gagal dihapus"}, status=400)
