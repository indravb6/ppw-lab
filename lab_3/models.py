from django.db import models


class Guest(models.Model):
    email = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
