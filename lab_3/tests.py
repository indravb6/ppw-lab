from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from urllib.parse import urlencode
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
from .models import Guest


class Lab10FunctionalTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(Lab10FunctionalTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab10FunctionalTestCase, self).tearDown()

    def test_add_data(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/lab10/register")

        email = selenium.find_element_by_id('email')
        name = selenium.find_element_by_id('name')
        password = selenium.find_element_by_id('password')
        submit = selenium.find_element_by_id('submit')

        email.send_keys('muhammad.indra71@ui.ac.id')
        name.send_keys('Indra')
        password.send_keys('password')

        submit.send_keys(Keys.RETURN)

        sleep(2)

        result = selenium.find_element_by_id('result')

        self.assertIn("Data berhasil disimpan",
                      result.get_attribute('innerHTML'))
        self.assertEqual("", email.get_attribute('value'))
        self.assertEqual("", name.get_attribute('value'))
        self.assertEqual("", password.get_attribute('value'))

        email.send_keys('muhammad.indra71@ui.ac.id')
        name.send_keys('Indra')
        password.send_keys('password')

        sleep(2)

        result = selenium.find_element_by_id('result')

        self.assertIn("Email sudah digunakan",
                      result.get_attribute('innerHTML'))

    def test_remove_data(self):
        guest = Guest(name='Indra',
                      email='muhammad.indra@ui.ac.id', password='PEPEW123')
        guest.save()

        selenium = self.selenium
        selenium.get(self.live_server_url + "/lab10/list")

        sleep(2)

        table = selenium.find_element_by_id('guest-list')
        self.assertIn(guest.email,
                      table.get_attribute('innerHTML'))
        self.assertIn(guest.name,
                      table.get_attribute('innerHTML'))

        delete_button = selenium.find_element_by_id('delete-' + str(guest.id))
        delete_button.click()

        sleep(1)

        password_field = selenium.find_element_by_id(
            'insert-password')
        password_field.send_keys(guest.password)
        confirm_button = selenium.find_element_by_id('confirm')
        confirm_button.click()

        sleep(2)

        selenium.switch_to_alert().accept()

        sleep(2)

        self.assertNotIn(guest.email,
                         table.get_attribute('innerHTML'))
        self.assertNotIn(guest.name,
                         table.get_attribute('innerHTML'))
