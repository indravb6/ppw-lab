from django.conf.urls import include
from django.contrib import admin
from django.urls import path, re_path
from .views import home
from lab_3.views import register_page, is_email_available, guest_list, get_list, delete_guest

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-3/', include('lab_3.urls')),
    re_path(r'^lab-5/', include('lab_5.urls')),
    path('lab10/register', register_page, name='register_page'),
    path('lab10/is-email-available', is_email_available, name='is_email_available'),
    path('lab10/list', guest_list, name='guest-list'),
    path('lab10/get-list', get_list, name='get-list'),
    path('lab10/delete-guest', delete_guest, name='delete-guest'),
    path('', home, name='home'),
]
