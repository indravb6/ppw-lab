from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Muhammad Indra Ramadhan'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 1, 12)
npm = '1706028695'
hobby = 'Playing game'
university = 'University of Indonesia'
description = "Aku mahasiswa semester 3 yang sedang berusaha untuk menjadi lebih rajin dan disiplin, lagi mau mencoba hal hal baru juga, mungkin baca buku"


def index(request):
    response = {'name': mhs_name, 'age': calculate_age(
        birth_date.year), 'npm': npm, 'hobby': hobby, 'university': university, 'description': description}
    return render(request, 'lab1_index.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
